FROM registry.hub.docker.com/gentoo/portage:latest AS portage
FROM registry.hub.docker.com/gentoo/stage3:amd64-desktop-systemd
COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo


RUN mkdir /tmp/img
WORKDIR /tmp/img
COPY ./ ./

RUN rm /etc/portage/make.profile
RUN cd /etc/portage && ln -sf ../../var/db/repos/gentoo/profiles/$([ -e "/tmp/img/profile" ] && cat /tmp/img/profile || cat /tmp/img/global/profile) /etc/portage/make.profile

RUN shopt -s dotglob && cp -r /tmp/img/config/config/* /etc/portage/
RUN shopt -s dotglob && cp -r /tmp/img/overlay/overlay/* /

RUN shopt -s dotglob && cp -r /tmp/img/config/local/* /etc/portage/
RUN shopt -s dotglob && cp -r /tmp/img/overlay/local/* /

RUN cat global/make.conf.append >> /etc/portage/make.conf

RUN emerge --quiet --getbinpkg --backtrack=1000 dev-vcs/git

RUN emaint sync -a

RUN emerge --jobs 12 --quiet --getbinpkg --changed-use --update --deep --backtrack=100 $(cat global/package.list) && emerge --jobs 12 --getbinpkg --changed-use --update --deep --backtrack=100 $(cat package.list)

RUN chmod +x global/fsscript.sh && chmod +x fsscript.sh && global/fsscript.sh && ./fsscript.sh


